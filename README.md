# Notes

### Setup

- `yarn install`
- `yarn start`

### Other tasks

- Lint: `yarn lint`
- Test: `yarn test`

### Dependencies used

- `Tailwind CSS` for styling
- `Craco` to override PostCSS config
- `Redux` and `Redux-Saga` for managing client state
- `Reselect` to compose selectors
- `redux-mock-store` for mocking the redux store on tests

### Hosted URL

https://orderbook-kappa.vercel.app
