import { Action } from "redux";

import { Grouping, Market, OrderTypes, RawOrder } from "types";

export const GET_FEED = "GET_FEED";
export const SET_ORDERS = "SET_ORDERS";
export const SET_GROUPING = "SET_GROUPING";
export const SET_ACTIVE_MARKET = "SET_ACTIVE_MARKET";
export const KILL_FEED = "KILL_FEED";
export const START_FEED = "START_FEED";
export const SET_ERROR = "SET_ERROR";

export type SetActiveMarketAction = Action<typeof SET_ACTIVE_MARKET> & {
  payload: Market;
};

export type SetOrdersAction = Action<typeof SET_ORDERS> & {
  payload: {
    orderType: OrderTypes;
    orders: RawOrder[];
    market: Market;
  };
};

export type SetGroupingAction = Action<typeof SET_GROUPING> & {
  payload: Grouping;
};

export type SetErrorAction = Action<typeof SET_ERROR> & {
  payload?: Error;
};

export const getFeed = (): Action<typeof GET_FEED> => ({
  type: GET_FEED,
});

export const killFeed = (): Action<typeof KILL_FEED> => ({
  type: KILL_FEED,
});

export const startFeed = (): Action<typeof START_FEED> => ({
  type: START_FEED,
});

export const setActiveMarket = (market: Market): SetActiveMarketAction => ({
  type: SET_ACTIVE_MARKET,
  payload: market,
});

export const setAskOrders = (
  askOrders: RawOrder[],
  market: Market
): SetOrdersAction => ({
  type: SET_ORDERS,
  payload: {
    orderType: "asks",
    orders: askOrders,
    market,
  },
});

export const setBidOrders = (
  bidOrders: RawOrder[],
  market: Market
): SetOrdersAction => ({
  type: SET_ORDERS,
  payload: {
    orderType: "bids",
    orders: bidOrders,
    market,
  },
});

export const setGrouping = (grouping: Grouping): SetGroupingAction => ({
  type: SET_GROUPING,
  payload: grouping,
});

export const setError = (error?: Error): SetErrorAction => ({
  type: SET_ERROR,
  payload: error,
});
