import { fork, put } from "redux-saga/effects";

import { startFeed as startFeedAction } from "./actions";
import { startFeed } from "./feed.saga";
import rootSaga from "./rootSaga";

describe("rootSaga", () => {
  it("should fork startFeed saga and put action to start it", () => {
    const generator = rootSaga();
    expect(generator.next().value).toEqual(fork(startFeed));
    expect(generator.next().value).toEqual(put(startFeedAction()));
  });
});
