import { buffers, SagaIterator, eventChannel, EventChannel } from "redux-saga";
import { take, put, call, fork, select, spawn } from "redux-saga/effects";

import createSocket from "api/createSocket";
import { activeMarketSelector } from "store/selectors";
import { Market, RawFeed } from "types";
import { getEvent } from "utils/events";

import {
  setBidOrders,
  setAskOrders,
  START_FEED,
  KILL_FEED,
  SET_ACTIVE_MARKET,
  setError,
} from "./actions";

export function createSocketChannel(
  socket: ReturnType<typeof createSocket>,
  market: Market
): EventChannel<RawFeed | Error> {
  return eventChannel<RawFeed | Error>((emit) => {
    socket.onopen = () => socket.send(getEvent("subscribe", market));
    socket.onmessage = (evt: MessageEvent) => emit(JSON.parse(evt.data));
    socket.onerror = () => emit(new Error());
    socket.onclose = (evt: CloseEvent) =>
      emit(
        new Error(
          evt.code === 1000 ? "Socket was forced to close" : "Socket was closed"
        )
      );

    const unsubscribe = () => socket.close();
    return unsubscribe;
  }, buffers.expanding());
}

export function* subscribeToActiveMarket(
  socket: ReturnType<typeof createSocket>
): SagaIterator {
  while (true) {
    yield take(SET_ACTIVE_MARKET);
    const activeMarket: Market = yield select(activeMarketSelector);
    const oldMarket = activeMarket === "PI_XBTUSD" ? "PI_ETHUSD" : "PI_XBTUSD";
    socket.send(getEvent("unsubscribe", oldMarket));
    socket.send(getEvent("subscribe", activeMarket));
  }
}

export function* killFeed(
  socket: ReturnType<typeof createSocket>
): SagaIterator {
  while (true) {
    yield take(KILL_FEED);
    socket.close(1000);
  }
}

export function* updateFeed(
  socketChannel: ReturnType<typeof createSocketChannel>
): SagaIterator {
  while (true) {
    try {
      const payload: RawFeed = yield take(socketChannel);

      if (payload.asks) {
        yield put(setAskOrders(payload.asks, payload.product_id));
      }
      if (payload.bids) {
        yield put(setBidOrders(payload.bids, payload.product_id));
      }
    } catch (e) {
      yield put(setError(e));
      socketChannel.close();
      throw e;
    }
  }
}

export function* watchFeed(): SagaIterator {
  const activeMarket: Market = yield select(activeMarketSelector);
  const socket: ReturnType<typeof createSocket> = yield call(createSocket);
  const socketChannel: ReturnType<typeof createSocketChannel> = yield call(
    createSocketChannel,
    socket,
    activeMarket
  );
  yield put(setError());
  yield fork(updateFeed, socketChannel);
  yield fork(subscribeToActiveMarket, socket);
  yield fork(killFeed, socket);
}

export function* startFeed(): SagaIterator {
  while (true) {
    yield take(START_FEED);
    yield spawn(watchFeed);
  }
}
