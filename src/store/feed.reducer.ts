import { State } from "types";

import {
  SetOrdersAction,
  SetGroupingAction,
  SET_GROUPING,
  SET_ORDERS,
  SetActiveMarketAction,
  SET_ACTIVE_MARKET,
  SetErrorAction,
  SET_ERROR,
} from "./actions";

const initialState: State = {
  PI_ETHUSD: {
    bids: {},
    asks: {},
    grouping: 0.05,
  },
  PI_XBTUSD: {
    bids: {},
    asks: {},
    grouping: 0.5,
  },
  activeMarket: "PI_XBTUSD",
};

export default function reducer(
  state: State = initialState,
  action:
    | SetOrdersAction
    | SetGroupingAction
    | SetActiveMarketAction
    | SetErrorAction
): State {
  switch (action.type) {
    case SET_ACTIVE_MARKET:
      return {
        ...state,
        activeMarket: action.payload,
      };
    case SET_ORDERS:
      const rawOrders = action.payload.orders;
      const orderType = action.payload.orderType;
      const market = action.payload.market;

      const orders = rawOrders.reduce(
        (acc, [price, size]) => ({
          ...acc,
          [price]: size > 0 ? { size, price } : undefined,
        }),
        {}
      );

      return {
        ...state,
        [market]: {
          ...state[market],
          [orderType]: Object.assign({}, state[market][orderType], orders),
        },
      };
    case SET_GROUPING:
      const grouping = action.payload;
      return {
        ...state,
        [state.activeMarket]: {
          ...state[state.activeMarket],
          grouping: grouping,
        },
      };
    case SET_ERROR:
      const error = action.payload;
      return {
        ...state,
        error: error?.message,
      };
    default:
      return state;
  }
}
