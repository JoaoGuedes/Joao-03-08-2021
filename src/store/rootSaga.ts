import { SagaIterator } from "redux-saga";
import { fork, put } from "redux-saga/effects";

import { startFeed as startFeedAction } from "store/actions";

import { startFeed } from "./feed.saga";

export default function* rootSaga(): SagaIterator {
  yield fork(startFeed);
  yield put(startFeedAction());
}
