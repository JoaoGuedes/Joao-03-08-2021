import { State } from "../types";

import {
  activeMarketSelector,
  groupingSelector,
  errorSelector,
  askOrdersSelector,
} from "./selectors";

describe("selectors", () => {
  describe("activeMarketSelector", () => {
    it("should return active market", () => {
      const input: State = {
        PI_XBTUSD: {
          grouping: 1,
          bids: {},
          asks: {},
        },
        PI_ETHUSD: {
          bids: {},
          asks: {},
          grouping: 0.05,
        },
        activeMarket: "PI_XBTUSD",
      };
      expect(activeMarketSelector(input)).toEqual("PI_XBTUSD");
    });
  });

  describe("groupingSelector", () => {
    it("should return grouping in active market", () => {
      const input: State = {
        PI_XBTUSD: {
          asks: {},
          bids: {},
          grouping: 1,
        },
        PI_ETHUSD: {
          bids: {},
          asks: {},
          grouping: 0.05,
        },
        activeMarket: "PI_XBTUSD",
      };
      expect(groupingSelector(input)).toEqual(1);
    });
  });

  describe("errorSelector", () => {
    it("should return error in state", () => {
      const input: State = {
        PI_XBTUSD: {
          asks: {},
          bids: {},
          grouping: 1,
        },
        PI_ETHUSD: {
          bids: {},
          asks: {},
          grouping: 0.05,
        },
        activeMarket: "PI_XBTUSD",
        error: "foo",
      };
      expect(errorSelector(input)).toEqual("foo");
    });
  });

  describe("askOrdersSelector", () => {
    it("should return ask orders sorted and with totals", () => {
      const input: State = {
        PI_XBTUSD: {
          asks: {
            100: {
              price: 100,
              size: 50,
              total: 0,
            },
            101: {
              price: 101,
              size: 25,
              total: 0,
            },
            300: {
              price: 300,
              size: 20,
              total: 0,
            },
            750: {
              price: 750,
              size: 20,
              total: 0,
            },
          },
          bids: {},
          grouping: 1,
        },
        PI_ETHUSD: {
          bids: {},
          asks: {},
          grouping: 0.05,
        },
        activeMarket: "PI_XBTUSD",
      };
      const output = [
        { price: 100, size: 75, total: 75 },
        { price: 300, size: 20, total: 95 },
        { price: 750, size: 20, total: 115 },
      ];
      expect(askOrdersSelector(input)).toEqual(output);
    });
  });
});
