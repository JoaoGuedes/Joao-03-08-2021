import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";

import reducer from "./feed.reducer";
import rootSaga from "./rootSaga";

export const sagaMiddleware = createSagaMiddleware({
  onError: (error) => {
    console.error("an error has occurred", error);
  },
});

const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;
