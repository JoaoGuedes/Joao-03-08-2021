import { State } from "types";

import {
  SetActiveMarketAction,
  SetErrorAction,
  SetGroupingAction,
  SetOrdersAction,
  SET_ACTIVE_MARKET,
  SET_ERROR,
  SET_GROUPING,
  SET_ORDERS,
} from "./actions";
import reducer from "./feed.reducer";

describe("feed.reducer", () => {
  it("should return state with new active market", () => {
    const state: State = {
      PI_XBTUSD: {
        bids: {},
        asks: {},
        grouping: 1,
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    const input: SetActiveMarketAction = {
      type: SET_ACTIVE_MARKET,
      payload: "PI_ETHUSD",
    };
    const expected: State = {
      PI_XBTUSD: {
        bids: {},
        asks: {},
        grouping: 1,
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_ETHUSD",
    };
    expect(reducer(state, input)).toEqual(expected);
  });

  it("should return state with new orders", () => {
    const state: State = {
      PI_XBTUSD: {
        bids: {},
        asks: {},
        grouping: 0.5,
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    const input: SetOrdersAction = {
      type: SET_ORDERS,
      payload: {
        market: "PI_XBTUSD",
        orderType: "bids",
        orders: [
          [100, 10],
          [200, 30],
          [400, 40],
          [500, 0],
        ],
      },
    };
    const expected = {
      PI_XBTUSD: {
        asks: {},
        grouping: 0.5,
        bids: {
          100: {
            size: 10,
            price: 100,
          },
          200: {
            size: 30,
            price: 200,
          },
          400: {
            size: 40,
            price: 400,
          },
          500: undefined,
        },
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    expect(reducer(state, input)).toEqual(expected);
  });

  it("should return state with merged orders", () => {
    const state: State = {
      PI_XBTUSD: {
        bids: {
          250: {
            size: 200,
            price: 250,
            total: 0,
          },
        },
        asks: {},
        grouping: 1,
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    const input: SetOrdersAction = {
      type: SET_ORDERS,
      payload: {
        market: "PI_XBTUSD",
        orderType: "bids",
        orders: [
          [100, 10],
          [200, 30],
          [500, 0],
        ],
      },
    };
    const expected = {
      PI_XBTUSD: {
        asks: {},
        grouping: 1,
        bids: {
          100: {
            size: 10,
            price: 100,
          },
          200: {
            size: 30,
            price: 200,
          },
          250: {
            size: 200,
            price: 250,
            total: 0,
          },
          500: undefined,
        },
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    expect(reducer(state, input)).toEqual(expected);
  });

  it("should return state with new grouping value on active market", () => {
    const state: State = {
      PI_XBTUSD: {
        grouping: 0.5,
        bids: {},
        asks: {},
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    const input: SetGroupingAction = {
      type: SET_GROUPING,
      payload: 1,
    };

    const expected: State = {
      PI_XBTUSD: {
        grouping: 1,
        bids: {},
        asks: {},
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    expect(reducer(state, input)).toEqual(expected);
  });

  it("should return state with error message", () => {
    const state: State = {
      PI_XBTUSD: {
        grouping: 1,
        bids: {},
        asks: {},
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
    };
    const input: SetErrorAction = {
      type: SET_ERROR,
      payload: new Error("foo"),
    };

    const expected = {
      PI_XBTUSD: {
        grouping: 1,
        bids: {},
        asks: {},
      },
      PI_ETHUSD: {
        bids: {},
        asks: {},
        grouping: 0.05,
      },
      activeMarket: "PI_XBTUSD",
      error: "foo",
    };
    expect(reducer(state, input)).toEqual(expected);
  });
});
