import { EventChannel } from "redux-saga";
import { select, fork, spawn, take, call, put } from "redux-saga/effects";

import createSocket from "api/createSocket";
import { RawFeed, Market } from "types";
import { SubscriptionStatus } from "utils/events";

import {
  START_FEED,
  setError,
  setAskOrders,
  setBidOrders,
  KILL_FEED,
  SET_ACTIVE_MARKET,
} from "./actions";
import {
  createSocketChannel,
  startFeed,
  subscribeToActiveMarket,
  updateFeed,
  killFeed,
  watchFeed,
} from "./feed.saga";
import { activeMarketSelector } from "./selectors";

const socket: WebSocket = {
  send: jest.fn(),
  close: jest.fn(),
  binaryType: "blob",
  bufferedAmount: 1024,
  extensions: "",
  onclose: jest.fn(),
  onerror: jest.fn(),
  onopen: jest.fn(),
  onmessage: jest.fn(),
  protocol: "",
  readyState: 0,
  url: "",
  CLOSED: 0,
  CLOSING: 0,
  CONNECTING: 0,
  OPEN: 0,
  addEventListener: jest.fn(),
  removeEventListener: jest.fn(),
  dispatchEvent: jest.fn(),
};

const socketChannel: EventChannel<RawFeed | Error> = {
  close: jest.fn(),
  flush: jest.fn(),
  take: jest.fn(),
};

jest.mock("utils/events", () => ({
  getEvent: (event: SubscriptionStatus, market: Market) => ({
    event,
    market,
  }),
}));

describe("feed.saga", () => {
  describe("startFeed", () => {
    it("should take action and spawn watchFeed saga", () => {
      const generator = startFeed();
      expect(generator.next().value).toEqual(take(START_FEED));
      expect(generator.next().value).toEqual(spawn(watchFeed));
    });
  });

  describe("watchFeed", () => {
    it("should call socket and socket channel creation functions, and fork sagas", () => {
      const generator = watchFeed();
      expect(generator.next().value).toEqual(select(activeMarketSelector));
      expect(generator.next("MARKET").value).toEqual(call(createSocket));
      expect(generator.next(socket).value).toEqual(
        call(createSocketChannel, socket, "MARKET")
      );
      expect(generator.next(socketChannel).value).toEqual(put(setError()));
      expect(generator.next().value).toEqual(fork(updateFeed, socketChannel));
      expect(generator.next().value).toEqual(
        fork(subscribeToActiveMarket, socket)
      );
      expect(generator.next().value).toEqual(fork(killFeed, socket));
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe("updateFeed", () => {
    it("should take socketChannel event and put orders", () => {
      const generator = updateFeed(socketChannel);
      const payload = {
        asks: [],
        bids: [],
        product_id: "PI_XBTUSD",
      };
      expect(generator.next().value).toEqual(take(socketChannel));
      expect(generator.next(payload).value).toEqual(
        put(setAskOrders([], "PI_XBTUSD"))
      );
      expect(generator.next(payload).value).toEqual(
        put(setBidOrders([], "PI_XBTUSD"))
      );
    });

    it("should take socketChannel event and skip unrecognized message", () => {
      const generator = updateFeed(socketChannel);
      const data = { foo: 123 };
      expect(generator.next().value).toEqual(take(socketChannel));
      expect(generator.next(data).value).toEqual(take(socketChannel));
    });

    it("should put error when socketChannel errors out and close socketChannel", () => {
      const generator = updateFeed(socketChannel);
      const err = new Error("error");
      expect(generator.next().value).toEqual(take(socketChannel));
      try {
        expect(generator?.throw?.(err).value).toEqual(put(setError(err)));
        generator.next();
        expect(socketChannel.close).toHaveBeenCalled();
      } catch (e) {}
    });
  });

  describe("killFeed", () => {
    it("should take action and close socket", () => {
      const generator = killFeed(socket);
      expect(generator.next().value).toEqual(take(KILL_FEED));
      generator.next();
      expect(socket.close).toHaveBeenCalled();
    });
  });

  describe("subscribeToActiveMarket", () => {
    it("should take action, unsubscribe from old market and subscribe to new one", () => {
      const generator = subscribeToActiveMarket(socket);
      expect(generator.next().value).toEqual(take(SET_ACTIVE_MARKET));
      generator.next();
      generator.next("PI_XBTUSD");
      expect(socket.send).toHaveBeenCalledWith({
        event: "unsubscribe",
        market: "PI_ETHUSD",
      });
      expect(socket.send).toHaveBeenCalledWith({
        event: "subscribe",
        market: "PI_XBTUSD",
      });
    });
  });
});
