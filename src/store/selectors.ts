import { createSelector, OutputSelector } from "reselect";

import { OrderTypes, OrderIndexType, State, Grouping } from "types";
import { withTotal, sort, aggregate } from "utils/order";

export const activeMarketSelector = (state: State): State["activeMarket"] =>
  state.activeMarket;

export const groupingSelector = (state: State): Grouping =>
  state[state.activeMarket].grouping;

export const errorSelector = (state: State): State["error"] => state.error;

export const activeOrdersByTypeSelectorFactory = (
  type: OrderTypes
): OutputSelector<
  State,
  OrderIndexType,
  (res: OrderIndexType) => OrderIndexType
> => {
  return createSelector(
    (state: State) => state[state.activeMarket][type],
    (type: OrderIndexType) => type
  );
};

export const computedOrdersSelector = (
  orders: OrderIndexType,
  grouping: Grouping
): ReturnType<typeof withTotal> => {
  const sorted = sort(orders);
  const aggregated = aggregate(sorted, grouping);
  return withTotal(aggregated);
};

export const askOrdersSelector = createSelector(
  activeOrdersByTypeSelectorFactory("asks"),
  groupingSelector,
  computedOrdersSelector
);

export const bidOrdersSelector = createSelector(
  activeOrdersByTypeSelectorFactory("bids"),
  groupingSelector,
  computedOrdersSelector
);
