import React from "react";

import ConnectedGroupSelect from "components/ConnectedGroupingSelect";
import Row from "components/Row";
import { Order } from "types";

export const Spread: React.FC<{ value: string }> = ({ value }) => (
  <span className="font-mono text-gray-500 font-semibold text-xs">
    Spread: {value}
  </span>
);

type TitleBarProps = {
  spread: string;
};

export const TitleBar: React.FC<TitleBarProps> = ({ spread }) => (
  <div className="flex justify-between items-center p-2 border-b sticky top-0 border-gray-600 text-white bg-gray-900 z-20">
    <div className="text-sm">Order Book</div>
    <div className="hidden sm:block">
      <Spread value={spread} />
    </div>
    <div>
      <ConnectedGroupSelect />
    </div>
  </div>
);

const _OrderRow: React.FC<React.ComponentProps<typeof Row.Body>> = (props) => (
  <Row.Body {...props} />
);

const OrderRow = React.memo(
  _OrderRow,
  (prev, next) =>
    prev.total === next.total &&
    prev.size === next.size &&
    prev.price === next.price &&
    prev.FillProps?.depth === next.FillProps?.depth
);

type OrderContainerProps = {
  orders: Order[];
  depthFn: (order: Order) => number;
};

export const Bids: React.FC<OrderContainerProps> = ({ orders, depthFn }) => (
  <div className="flex flex-1 flex-col">
    <Row.Header className="flex-row-reverse sm:flex sm:flex-row border-b border-gray-800 sticky top-10 bg-gray-900 z-10" />
    {orders.map((order) => (
      <OrderRow
        dataTestId={`bid-${order.price}-${order.size}-${order.total}`}
        key={order.price}
        price={order.price}
        size={order.size}
        total={order.total || 0}
        className="flex-row-reverse sm:flex-row"
        TotalProps={{ className: "text-green-500" }}
        FillProps={{
          className: "left-0 sm:left-auto sm:right-0 bg-green-900",
          depth: `${depthFn(order)}%`,
        }}
      />
    ))}
  </div>
);

export const Asks: React.FC<OrderContainerProps> = ({ orders, depthFn }) => (
  <div className="flex flex-1 flex-col-reverse sm:flex-col">
    <Row.Header className="flex-row-reverse order-1 sm:order-none border-b border-gray-800 sticky top-10 bg-gray-900 z-10" />
    {orders.map((order) => (
      <OrderRow
        dataTestId={`ask-${order.price}-${order.size}-${order.total}`}
        key={order.price}
        price={order.price}
        size={order.size}
        total={order.total || 0}
        className="flex-row-reverse"
        TotalProps={{ className: "text-red-500" }}
        FillProps={{
          className: "left-0 bg-red-900",
          depth: `${depthFn(order)}%`,
        }}
      />
    ))}
  </div>
);

type ErrorProps = {
  error: string;
};

export const Error: React.FC<ErrorProps> = ({ error }) => (
  <div className="flex items-center justify-center p-2">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className="h-5 w-5 text-red-700"
      viewBox="0 0 20 20"
      fill="currentColor"
    >
      <path
        fillRule="evenodd"
        d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7 4a1 1 0 11-2 0 1 1 0 012 0zm-1-9a1 1 0 00-1 1v4a1 1 0 102 0V6a1 1 0 00-1-1z"
        clipRule="evenodd"
      />
    </svg>
    <h1 className="text-white ml-2">{error}</h1>
  </div>
);
