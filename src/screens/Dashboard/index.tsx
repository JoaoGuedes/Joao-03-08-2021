import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";

import { killFeed, startFeed, setActiveMarket } from "store/actions";
import {
  activeMarketSelector,
  askOrdersSelector,
  bidOrdersSelector,
  errorSelector,
} from "store/selectors";
import { Order } from "types";
import { calcDepth, calcSpread } from "utils/order";

import { TitleBar, Bids, Asks, Error, Spread } from "./Dashboard.components";

const Dashboard: React.FC = () => {
  const dispatch = useDispatch();
  const asks = useSelector(askOrdersSelector);
  const bids = useSelector(bidOrdersSelector);
  const error = useSelector(errorSelector);
  const activeMarket = useSelector(activeMarketSelector);
  const spread = calcSpread(asks[0]?.price, bids[0]?.price);
  const depthFn = useCallback(
    (order: Order) =>
      calcDepth(
        order,
        asks[asks.length - 1]?.total,
        bids[bids.length - 1]?.total
      ),
    [asks, bids]
  );
  const toggle = () =>
    dispatch(
      setActiveMarket(activeMarket === "PI_XBTUSD" ? "PI_ETHUSD" : "PI_XBTUSD")
    );
  const kill = () => dispatch(killFeed());
  const start = () => dispatch(startFeed());

  return (
    <div>
      <div className="overflow-scroll" style={{ maxHeight: "90vh" }}>
        <TitleBar spread={spread} />
        <div className="flex flex-col-reverse sm:flex-row ">
          <Bids orders={bids} depthFn={depthFn} />
          <div className="block sm:hidden text-white text-center">
            <Spread value={spread} />
          </div>
          <Asks orders={asks} depthFn={depthFn} />
        </div>
      </div>
      <div className="fixed bottom-0 w-full py-1 flex flex-col items-center justify-center z-30 bg-gray-900">
        <div>{error && <Error error={error} />}</div>
        <div>
          <button
            onClick={toggle}
            className="bg-indigo-600 hover:bg-indigo-800 text-white py-2 px-4 rounded"
          >
            Toggle Feed
          </button>
          <button
            onClick={error ? start : kill}
            className="bg-red-700 hover:bg-red-800 text-white py-2 px-8 ml-3 rounded"
          >
            {error ? "Start Feed" : "Kill Feed"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
