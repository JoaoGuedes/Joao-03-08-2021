import { fireEvent, render } from "@testing-library/react";
import { Provider } from "react-redux";
import * as redux from "react-redux";
import configureStore from "redux-mock-store";

import { killFeed, setActiveMarket, startFeed } from "store/actions";
import { State } from "types";

import Dashboard from "./";

const state: State = {
  PI_XBTUSD: {
    asks: {
      100: {
        price: 100,
        size: 10,
        total: 0,
      },
      101: {
        price: 101,
        size: 20,
        total: 0,
      },
      105: {
        price: 105,
        size: 20,
        total: 0,
      },
    },
    bids: {
      98: {
        price: 98,
        size: 10,
        total: 0,
      },
      101: {
        price: 101,
        size: 20,
        total: 0,
      },
      105: {
        price: 105,
        size: 50,
        total: 0,
      },
    },
    grouping: 1,
  },
  PI_ETHUSD: {
    asks: {
      10: {
        price: 10,
        size: 10,
        total: 0,
      },
      11: {
        price: 11,
        size: 20,
        total: 0,
      },
      15: {
        price: 15,
        size: 20,
        total: 0,
      },
    },
    bids: {
      20: {
        price: 20,
        size: 10,
        total: 0,
      },
      21: {
        price: 21,
        size: 20,
        total: 0,
      },
      25: {
        price: 25,
        size: 50,
        total: 0,
      },
    },
    grouping: 0.05,
  },
  activeMarket: "PI_XBTUSD",
};

const mockStore = configureStore();

describe("Dashboard", () => {
  it("should render correct spread label", () => {
    const store = mockStore(state);
    const { getAllByText } = render(
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
    expect(getAllByText("Spread: 2.00 (2.00%)", { exact: false })).toHaveLength(
      2
    );
  });

  it("should render orders with correct values and depth", () => {
    const store = mockStore(state);
    const { getByTestId } = render(
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
    expect(getByTestId("bid-98-10-10")).toBeDefined();
    expect(getByTestId("bid-98-10-10-depth-12.5%")).toBeDefined();
    expect(getByTestId("bid-101-20-30")).toBeDefined();
    expect(getByTestId("bid-101-20-30-depth-37.5%")).toBeDefined();
    expect(getByTestId("bid-105-50-80")).toBeDefined();
    expect(getByTestId("bid-105-50-80-depth-100%")).toBeDefined();

    expect(getByTestId("ask-100-30-30")).toBeDefined();
    expect(getByTestId("ask-100-30-30-depth-37.5%")).toBeDefined();
    expect(getByTestId("ask-105-20-50")).toBeDefined();
    expect(getByTestId("ask-105-20-50-depth-62.5%")).toBeDefined();
  });

  it("should dispatch setActiveMarket action on pressing 'Toggle Feed'", () => {
    const store = mockStore(state);
    const useDispatchSpy = jest.spyOn(redux, "useDispatch");
    const mockDispatch = jest.fn();
    useDispatchSpy.mockReturnValue(mockDispatch);
    const { getByText } = render(
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
    fireEvent.click(getByText("Toggle Feed"));
    expect(mockDispatch).toHaveBeenCalledWith(setActiveMarket("PI_ETHUSD"));
  });

  it("should dispatch killFeed action on pressing 'Kill Feed'", () => {
    const store = mockStore(state);
    const useDispatchSpy = jest.spyOn(redux, "useDispatch");
    const mockDispatch = jest.fn();
    useDispatchSpy.mockReturnValue(mockDispatch);
    const { getByText } = render(
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
    fireEvent.click(getByText("Kill Feed"));
    expect(mockDispatch).toHaveBeenCalledWith(killFeed());
  });

  it("should dispatch startFeed action on pressing 'Start Feed'", () => {
    const store = mockStore({
      ...state,
      error: "error",
    });
    const useDispatchSpy = jest.spyOn(redux, "useDispatch");
    const mockDispatch = jest.fn();
    useDispatchSpy.mockReturnValue(mockDispatch);
    const { getByText } = render(
      <Provider store={store}>
        <Dashboard />
      </Provider>
    );
    fireEvent.click(getByText("Start Feed"));
    expect(mockDispatch).toHaveBeenCalledWith(startFeed());
  });
});
