export type OrderTypes = "bids" | "asks";

export type Market = "PI_XBTUSD" | "PI_ETHUSD";

export type GroupingXBT = 0.5 | 1 | 2.5;

export type GroupingETH = 0.05 | 0.1 | 0.25;

export type Grouping = GroupingXBT | GroupingETH;

export type RawOrder = [number, number];

export type Order = {
  price: number;
  total: number;
  size: number;
};

export type RawFeed = {
  feed: string;
  product_id: Market;
  bids: RawOrder[];
  asks: RawOrder[];
};

export type OrderIndexType = {
  [price: number]: Order;
};

export type State = {
  PI_XBTUSD: {
    [k in OrderTypes]: OrderIndexType;
  } & { grouping: GroupingXBT };
  PI_ETHUSD: {
    [k in OrderTypes]: OrderIndexType;
  } & { grouping: GroupingETH };
  activeMarket: Market;
  error?: string;
};
