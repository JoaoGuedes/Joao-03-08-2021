import { GroupingETH } from "types";

import { GroupingXBT } from "./../types";

export const GroupsXBT: GroupingXBT[] = [0.5, 1, 2.5];
export const GroupsETH: GroupingETH[] = [0.05, 0.1, 0.25];
