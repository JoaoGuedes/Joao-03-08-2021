import { Order, OrderIndexType, Grouping } from "types";

export const sort = (orders: OrderIndexType): Order[] =>
  Object.values(orders)
    .sort((a, b) => a.price - b.price)
    .filter((v) => !!v);

export const withTotal = (orders: Order[]): Order[] => {
  const clonedOrders: Order[] = JSON.parse(JSON.stringify(orders));
  for (let i = 0; i < clonedOrders.length; i++) {
    clonedOrders[i].total =
      clonedOrders[i]?.size + (clonedOrders[i - 1]?.total || 0);
  }
  return clonedOrders;
};

const sum = (orders: Order[]) =>
  orders.reduce(
    (acc, curr) => ({
      price: acc.price,
      size: acc.size + curr.size,
      total: 0,
    }),
    {
      price: orders[0].price,
      size: 0,
      total: 0,
    }
  );

const delta = (orderA: Order, orderB: Order) =>
  Math.abs(orderA.price - orderB.price);

export const aggregate = (
  orders: Order[] = [],
  grouping: Grouping,
  accumulator: Order[] = []
): Order[] => {
  if (!orders.length) {
    return [];
  }
  const referenceOrder = orders[0];
  const nextOrders = orders.slice(1);
  const localAcc = [referenceOrder];
  for (let i = 0; i < nextOrders.length; i++) {
    if (delta(nextOrders[i], referenceOrder) > grouping) {
      return aggregate(nextOrders.slice(i), grouping, [
        ...accumulator,
        sum(localAcc),
      ]);
    }
    localAcc.push(nextOrders[i]);
  }
  return [...accumulator, sum(localAcc)];
};

export const calcDepth = (
  order: Order,
  totalAsks?: number,
  totalBids?: number
): number =>
  ((order.total || 0) / Math.max(totalAsks || 0, totalBids || 0)) * 100;

export const calcSpread = (orderA: number, orderB: number): string => {
  const spread = Math.abs(orderA - orderB);
  const relativeSpread = (spread / Math.max(orderA, orderB)) * 100;
  return `${spread.toFixed(2)} (${relativeSpread.toFixed(2)}%)`;
};
