import { Market } from "../types";

export type SubscriptionStatus = "subscribe" | "unsubscribe";

export const getEvent = (event: SubscriptionStatus, market: Market): string =>
  JSON.stringify({
    event,
    feed: "book_ui_1",
    product_ids: [market],
  });
