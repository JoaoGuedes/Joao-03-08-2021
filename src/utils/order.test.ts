import { Order } from "types";

import { aggregate, sort, withTotal, calcDepth, calcSpread } from "./order";

describe("utils", () => {
  describe("sort", () => {
    it("should sort orders by ascending price", () => {
      const input = {
        200: {
          price: 200,
          size: 10,
          total: 0,
        },
        100: {
          price: 100,
          size: 10,
          total: 0,
        },
      };
      const expected = [
        {
          price: 100,
          size: 10,
          total: 0,
        },
        {
          price: 200,
          size: 10,
          total: 0,
        },
      ];
      expect(sort(input)).toEqual(expected);
    });
  });

  describe("withTotal", () => {
    it("should accumulate totals in orders", () => {
      const input = [
        {
          price: 100,
          size: 10,
          total: 0,
        },
        {
          price: 200,
          size: 10,
          total: 0,
        },
        {
          price: 300,
          size: 20,
          total: 0,
        },
      ];
      const expected = [
        {
          price: 100,
          size: 10,
          total: 10,
        },
        {
          price: 200,
          size: 10,
          total: 20,
        },
        {
          price: 300,
          size: 20,
          total: 40,
        },
      ];
      expect(withTotal(input)).toEqual(expected);
    });
  });

  describe("aggregate", () => {
    it("should aggregate correctly", () => {
      const orders: Order[] = [
        {
          price: 100,
          size: 10,
          total: 0,
        },
        {
          price: 100.5,
          size: 10,
          total: 0,
        },
        {
          price: 103,
          size: 10,
          total: 0,
        },
        {
          price: 107,
          size: 10,
          total: 0,
        },
        {
          price: 109.5,
          size: 10,
          total: 0,
        },
      ];

      const output: Order[] = [
        {
          price: 100,
          size: 20,
          total: 0,
        },
        {
          price: 103,
          size: 10,
          total: 0,
        },
        {
          price: 107,
          size: 20,
          total: 0,
        },
      ];
      expect(aggregate(orders, 2.5)).toEqual(output);
    });
  });

  describe("calcDepth", () => {
    it("should calculate depth of order total in relation to biggest total", () => {
      const order = {
        price: 200,
        size: 10,
        total: 1000,
      };
      const totalAsks = 2000;
      const totalBids = 10000;
      expect(calcDepth(order, totalAsks, totalBids)).toEqual(10);
    });
  });

  describe("calcSpread", () => {
    it("should calculate spread between two values", () => {
      expect(calcSpread(200, 400)).toEqual(`200.00 (50.00%)`);
    });
  });
});
