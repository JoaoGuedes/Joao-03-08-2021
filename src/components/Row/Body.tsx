type Props = {
  dataTestId?: string;
  total: number;
  size: number;
  price: number;
  className?: string;
  TotalProps: {
    className?: string;
  };
  FillProps: {
    className?: string;
    depth?: string;
  };
};

const Body: React.FC<Props> = ({
  dataTestId,
  total,
  size,
  price,
  className,
  TotalProps: { className: totalClassName },
  FillProps: { depth = "0%", className: fillClassName = "right-0" },
}) => (
  <div
    className={`relative flex w-full p-1 text-right ${className}`}
    data-testid={dataTestId}
  >
    <div className="flex-1 text-gray-100 font-mono text-xs">{total}</div>
    <div className="flex-1 text-gray-100 font-mono text-xs">{size}</div>
    <div className={`flex-1 font-mono text-xs ${totalClassName}`}>{price}</div>
    <div
      className={`h-full absolute top-0 opacity-50 ${fillClassName}`}
      style={{ width: depth }}
      data-testid={`${dataTestId}-depth-${depth}`}
    />
  </div>
);

export default Body;
