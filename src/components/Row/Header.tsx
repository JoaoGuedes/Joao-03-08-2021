type Props = {
  className: string;
};

const Header: React.FC<Props> = ({ className }) => (
  <div className={`flex p-1 text-right ${className}`}>
    <div className="flex-1 text-gray-500 text-xs font-mono font-semibold uppercase">
      total
    </div>
    <div className="flex-1 text-gray-500 text-xs font-mono font-semibold uppercase">
      size
    </div>
    <div className="flex-1 text-gray-500 text-xs font-mono font-semibold uppercase">
      price
    </div>
  </div>
);

export default Header;
