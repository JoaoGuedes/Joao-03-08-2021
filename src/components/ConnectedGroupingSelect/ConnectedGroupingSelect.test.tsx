import { fireEvent, render } from "@testing-library/react";
import { Provider } from "react-redux";
import * as redux from "react-redux";
import configureStore from "redux-mock-store";

import { setGrouping } from "store/actions";
import { State } from "types";
import { GroupsETH, GroupsXBT } from "utils/constants";

import ConnectedGroupingSelect from "./";

const state: State = {
  PI_XBTUSD: {
    asks: {},
    bids: {},
    grouping: 1,
  },
  PI_ETHUSD: {
    asks: {},
    bids: {},
    grouping: 0.05,
  },
  activeMarket: "PI_XBTUSD",
};

const mockStore = configureStore();

describe("ConnectedGroupingSelect", () => {
  it("should render correct options for XBT", () => {
    const store = mockStore(state);
    const { getByText } = render(
      <Provider store={store}>
        <ConnectedGroupingSelect />
      </Provider>
    );
    GroupsXBT.forEach((group) => {
      expect(getByText(`Group ${group}`)).toBeDefined();
    });
  });

  it("should render correct options for ETH", () => {
    const store = mockStore({
      ...state,
      activeMarket: "PI_ETHUSD",
    });
    const { getByText } = render(
      <Provider store={store}>
        <ConnectedGroupingSelect />
      </Provider>
    );
    GroupsETH.forEach((group) => {
      expect(getByText(`Group ${group}`)).toBeDefined();
    });
  });

  it("should dispatch setGrouping action on changing group", () => {
    const store = mockStore(state);
    const useDispatchSpy = jest.spyOn(redux, "useDispatch");
    const mockDispatch = jest.fn();
    useDispatchSpy.mockReturnValue(mockDispatch);
    const { getByTestId } = render(
      <Provider store={store}>
        <ConnectedGroupingSelect />
      </Provider>
    );
    fireEvent.change(getByTestId("grouping-select"), {
      target: { value: 0.5 },
    });
    expect(mockDispatch).toHaveBeenCalledWith(setGrouping(0.5));
  });
});
