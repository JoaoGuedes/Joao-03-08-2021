import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { setGrouping } from "store/actions";
import { activeMarketSelector, groupingSelector } from "store/selectors";
import { Grouping } from "types";
import { GroupsETH, GroupsXBT } from "utils/constants";

const ConnectedGroupingSelect: React.FC = () => {
  const dispatch = useDispatch();
  const grouping = useSelector(groupingSelector);
  const activeMarket = useSelector(activeMarketSelector);
  const groups = activeMarket === "PI_XBTUSD" ? GroupsXBT : GroupsETH;
  const onChange = (evt: React.ChangeEvent<HTMLSelectElement>) => {
    const value = parseFloat(evt.target.value) as Grouping;
    dispatch(setGrouping(value));
  };

  return (
    <select
      data-testid="grouping-select"
      onChange={onChange}
      value={grouping}
      className="inline-flex justify-center w-full rounded-md shadow-sm px-2 py-1 bg-gray-700 text-sm font-medium text-blue-100"
    >
      {groups.map((group) => (
        <option key={group} value={group}>
          Group {group}
        </option>
      ))}
    </select>
  );
};

export default ConnectedGroupingSelect;
