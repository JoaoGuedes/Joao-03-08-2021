import { URL } from "./constants";

const createSocket = (): WebSocket => new WebSocket(URL);

export default createSocket;
