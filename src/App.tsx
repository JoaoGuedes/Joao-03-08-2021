import React from "react";
import { Provider } from "react-redux";

import Dashboard from "screens/Dashboard";
import store from "store";

const App: React.FC = () => (
  <Provider store={store}>
    <Dashboard />
  </Provider>
);

export default App;
